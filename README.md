# Usage

Single threaded, 100 items per batch:

  drush mr-i --group=my_group --batch=100

Multi-threaded, 100 itemas per batch, 4 threads:

  drush mr-i --group=my_group --batch=100 --threads=4 --config=modules/custom/migrate_runner

The `config` option points to the module's directory for drush can consume `drushrc.php` there
which contains our version Drush Logger. Its only purpose is to ignore error messages
on SQL deadlocks. Without it you'll be probably getting loads of them thinking that something is
broken while deadlocks are actually handled: there's a local version of `MigrateExecutable` class
which restarts transactions in case of deadlocks. It's worth to say it's invoked only when
you're using threads.

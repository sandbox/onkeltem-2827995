<?php

namespace Drupal\migrate_runner\fake;

use Drush\Log\LogLevel;
use Drush\Log\Logger as LoggerBase;

// Overrding the default Drush logger
$drush_logger = new Logger();
drush_set_context('DRUSH_LOG_CALLBACK', $drush_logger);

class Logger extends LoggerBase {

  public function log($level, $message, array $context = array()) {

    // Tweak: we're lowering down the severity level of deadlock messages
    // to hide them and not confuse the user
    if (preg_match('~SQLSTATE\[40001\]:.+1213 Deadlock found~', $message)) {
      $level = LogLevel::INFO;
    }

    parent::log($level, $message, $context);
  }


}

<?php

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_runner\MigrateRunner;
use Drupal\migrate_tools\DrushLogMigrateMessage;
use Drupal\migrate_tools\MigrateExecutable as MigrateToolsExecutable;
use Drupal\migrate_runner\MigrateExecutable as MigrateRunnerExecutable;

/**
 * Implements hook_drush_command().
 */
function migrate_runner_drush_command() {
  $items['migrate-runner-import'] = [
    'description' => 'Runs one or more migration.',
    'arguments' => [
      'migration' => 'Restrict to a comma-separated list of migrations. Optional',
    ],
    'options' => [
      'all' => 'Process all migrations.',
      'group' => 'A comma-separated list of migration groups to import',
      'tag' => 'Name of the migration tag to import',
      'feedback' => 'Frequency of progress messages, in items processed',
      'batch' => 'The number of records in a batch',
      'threads' => 'The number of threads',
    ],
    'aliases' => ['mr-i'],
    'drupal dependencies' => ['migrate_runner'],
  ];
  $items['migrate-runner-job'] = [
    'description' => 'Runs a single migration.',
    'arguments' => [
      'migration' => 'Migration ID',
    ],
    'options' => [
      'job' => 'A background job id',
      'offset' => 'The offset',
      'batch' => 'The number of records in a batch',
      'solo' => 'Indicates that there no threads running in parallel',
      'feedback' => 'Frequency of progress messages, in items processed',
    ],
    'aliases' => ['mr-j'],
    'drupal dependencies' => ['migrate_runner'],
  ];

  return $items;
}

/**
 * @param string $migration_names
 */
function drush_migrate_runner_import($migration_names = '') {

  // Starting a timer
  $started = _drush_migrate_runner_get_time();

  $group_names = drush_get_option('group');
  $tag_names = drush_get_option('tag');
  $all = drush_get_option('all');

  if (!$all && !$group_names && !$migration_names && !$tag_names) {
    drush_set_error('MIGRATE_ERROR', dt('You must specify --all, --group, --tag, or one or more migration names separated by commas'));
    return;
  }

  $options = _drush_migrate_runner_get_options();

  $migrations = drush_migrate_tools_migration_list($migration_names);
  if (empty($migrations)) {
    drush_log(dt('No migrations found.'), 'error');
  }

  try {
    foreach ($migrations as $migration_group => $migration_list) {
      $runner = new MigrateRunner($migration_list, $options);
      $runner->import();
      unset($runner);
    }
  }
  finally {
    $diff_sec = (_drush_migrate_runner_get_time() - $started) / 1000000;
    $hours = floor($diff_sec / 3600);
    $minutes = floor($diff_sec % 3600 / 60);
    $seconds = floor($diff_sec % 3600 % 60);
    drush_print(sprintf('Time taken: %02d:%02d:%02d', $hours, $minutes, $seconds));
  }

}

function _drush_migrate_runner_get_time() {
  return round(microtime(TRUE) * 1000000);
}

/**
 * Run a single migration.
 * @param $migration_id
 * @return bool
 */
function drush_migrate_runner_job($migration_id) {
  $job_id = drush_get_option('job');
  $offset = drush_get_option('offset');

  if (!$migration_id || !$job_id) {
    return FALSE;
  }

  // Load migration
  $manager = \Drupal::service('plugin.manager.config_entity_migration');
  /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
  if (!($migration = $manager->createInstance($migration_id))) {
    return FALSE;
  }

  drush_print('Launching ' . $migration_id . ', job_id: ' . $job_id . ', offset: ' . $offset);

  $log = new DrushLogMigrateMessage();
  $options = _drush_migrate_runner_get_options();
  $executable = drush_get_option('solo') ? new MigrateToolsExecutable($migration, $log, $options) : new MigrateRunnerExecutable($migration, $log, $options);
  $ret_code = drush_op([$executable, 'import']);

  $reasons = [
    // Passed
    MigrationInterface::RESULT_COMPLETED => 'Migration is completed',
    MigrationInterface::RESULT_DISABLED => 'Migration is disabled',
    // Error
    MigrationInterface::RESULT_INCOMPLETE => 'Incomplete migration',
    MigrationInterface::RESULT_STOPPED => 'Migration is stopped',
    MigrationInterface::RESULT_FAILED => 'Migration has failed',
    MigrationInterface::RESULT_SKIPPED => 'Migrations is skipped due to unfulfilled dependencies',
  ];

  if (!in_array($ret_code, [MigrationInterface::RESULT_COMPLETED, MigrationInterface::RESULT_DISABLED])) {
    \Drupal::keyValue('migrate_runner_errors')->set('job_' . $job_id, TRUE);
    drush_log(dt('Migration is failed with reason: @reason', ['@reason' => isset($reasons[$ret_code]) ? $reasons[$ret_code] : 'Unknown']), \Drush\Log\LogLevel::ERROR);
  }

  // Remove the job
  \Drupal::keyValue('migrate_runner')->delete('job_' . $job_id);
  drush_print('Finished ' . $migration_id . ', job_id: ' . $job_id . ', offset: ' . $offset);

  return NULL;
}

function _drush_migrate_runner_get_options() {
  $options = [];
  foreach (['feedback', 'batch', 'threads', 'config'] as $option) {
    if (drush_get_option($option)) {
      $options[$option] = drush_get_option($option);
    }
  }
  return $options;
}

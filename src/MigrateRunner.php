<?php

namespace Drupal\migrate_runner;

use Drupal\Core\Database\Query\Select;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Psr\Log\LogLevel;

class MigrateRunner {

  /**
   * @var \Drupal\migrate\Plugin\Migration[]
   */
  protected $migrationList;

  /** @var  array */
  protected $options;

  /**
   * Default runner options
   *
   * @var array  */
  protected static $optionDefaults = [
    'batch' => 1000,
    // Drupal Core doesn't support more then ONE thread. Use it at your own risk.
    'threads' => 1,
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $migrations, $options = []) {
    $this->migrationList = $migrations;
    $this->options = $options + static::$optionDefaults;
  }

  /**
   * Run the configured migrations.
   */
  public function import() {
    $this->resetJobs();
    $error = FALSE;
    foreach ($this->migrationList as $migration_id => $migration) {
      if ($error) {
        break;
      }
      $offset = 0;
      // It's only possible to split an SQL-based migration
      if ($migration->getSourcePlugin() instanceof SqlBase) {
        $records_to_process = $migration->getSourcePlugin()->count();
        $jobs_count = 0;

        while (TRUE) {
          // Run migrate jobs until out of threads or records
          $job_id = 0;
          while ($jobs_count < $this->options['threads'] && $records_to_process > 0) {
            // Add newline
            if ($job_id) {
              drush_print('');
            }
            $job_id++;
            $jobs_count++;

            drush_print_prompt('Forking ' . $migration_id . ', job_id: ' . $job_id . ', offset: ' . $offset);
            $process_options = [
                'job' => $job_id,
                'offset' => $offset,
                'solo' => $this->options['threads'] == 1,
              ] + array_intersect_key($this->options, array_flip(['feedback', 'batch', 'config']));
            // Push job before running it
            \Drupal::keyValue('migrate_runner')->set('job_' . $job_id, $job_id);
            drush_invoke_process('@self', 'mr-j', [$migration_id], $process_options, ['fork' => TRUE]);

            $offset += $this->options['batch'];
            $records_to_process -= $this->options['batch'];
          }

          // If there's no more jobs, continuing
          if (!$jobs_count) {
            break;
          }

          drush_print_prompt(' ');
          // Out of threads or records, in any case wait for the jobs to finish
          while (TRUE) {
            if (!$this->getJobsCount()) {
              $jobs_count = 0;
              break;
            }
            drush_print_prompt('.');
            sleep(1);
          };

          // Add a new line
          drush_print('');
          // Now when all jobs are done, check for errors.
          if ($this->getJobsErrors()) {
            drush_log(dt('Exiting due to migration errors.'), LogLevel::ERROR);
            $error = TRUE;
            break;
          }
        }
      }
      // Non-SQL migrations
      else {
        drush_print('Launching non-sql migration ' . $migration_id);
        $process_options = array_intersect_key($this->options, array_flip(['feedback']));
        drush_invoke_process('@self', 'mi', [$migration_id], $process_options);
      }
    }
  }

  protected function getJobsCount() {
    /** @var Select $jobs_query */
    $jobs_query = \Drupal::database()->select('key_value', 'kv')
      ->fields('kv', ['collection'])
      ->condition('collection', 'migrate_runner');
    return $jobs_query->countQuery()->execute()->fetchField();
  }

  protected function getJobsErrors() {
    /** @var Select $jobs_query */
    $jobs_query = \Drupal::database()->select('key_value', 'kv')
      ->fields('kv', ['collection'])
      ->condition('collection', 'migrate_runner_errors');
    return $jobs_query->countQuery()->execute()->fetchField();
  }

  /**
   * Reset job counts and possible error flag
   */
  protected function resetJobs() {
    \Drupal::keyValue('migrate_runner')->deleteAll();
    \Drupal::keyValue('migrate_runner_errors')->deleteAll();
  }

}
